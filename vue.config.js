module.exports = {
  publicPath: '/',
  // Load styles
  // css: {
  //   loaderOptions: {
  //     sass: {
  //       data: `
  //         @import "@/styles/index.scss";
  //       `,
  //     },
  //   },
  // },
  filenameHashing: true,
  productionSourceMap: false,
  chainWebpack(config) {
    // Do not convert images to base64:
    // https://stackoverflow.com/a/51859212
    // https://cli.vuejs.org/guide/webpack.html#replacing-loaders-of-a-rule
    // https://github.com/vuejs/vue-cli/blob/dev/packages/%40vue/cli-service/lib/config/base.js#L106
    // Hack:
    //
    // 1. Get SVG rules
    const svgRule = config.module.rule('svg');
    svgRule
      .use('file-loader')
      .loader('file-loader')
      .tap(svg_options => {
        // console.log('svgRule options:', svg_options);

        const imagesRule = config.module.rule('images');

        imagesRule.uses.clear();
        imagesRule
          .test(/\.(png|jpe?g|gif|webp)(\?.*)?$/)
          .use('file-loader')
          .loader('file-loader')
          .tap(options => {
            return {
              // 2. Copy SVG rules to images rules
              ...svg_options,
              ...options,
              limit: -1, // no limit
            };
          })
          .end();

        return {
          // 3. Set SVG rules back
          ...svg_options,
        };
      })
      .end();

    // Fix caching issues
    // https://github.com/vuejs/vue-cli/issues/2051#issuecomment-474963166
    config.plugins.delete('preload');
  },
};
