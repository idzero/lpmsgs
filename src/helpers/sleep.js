/**
 * Pause async function
 * @param milliseconds
 * @returns {Promise<unknown>}
 */
const sleep = (milliseconds) => {
  return new Promise(resolve => setTimeout(resolve, milliseconds));
};

export default sleep;
