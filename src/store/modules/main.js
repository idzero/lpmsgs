import faker from 'faker';
import dayjs from 'dayjs';
import 'dayjs/locale/ru';
import sleep from '@/helpers/sleep';

// Generate same faker values on every run.
faker.seed(5000);

faker.locale = 'ru';

dayjs.locale('ru');

const LOAD_AMOUNT = 10;
const THREADS_AMOUNT = 100;
const MESSAGES_AMOUNT = 100;
const EXCERPT_LENGTH = 140;
const SELF_NAME = 'Вы';

const STATE = {
  fakeLoaded: false,
  threadLoaded: false,
  allThreads: [],
  allMessages: [],
  loadedThreads: LOAD_AMOUNT,
  loadingThreads: false,
  currentThreadId: null,
  loadingMessages: false,
  sendingMessage: false,
  showSidebar: false,
};

const sortThreadsByDate = (a, b) => new Date(b.latestDate).getTime() - new Date(a.latestDate).getTime()
const sortMessagesByDate = (a, b) => new Date(a.created).getTime() - new Date(b.created).getTime();

export default {
  namespaced: true,
  state: STATE,
  getters: {
    sidebarOpened: state => state.showSidebar || !state.currentThreadId,
    visibleThreads: state => [...state.allThreads]
      .splice(0, state.loadedThreads)
      // Add excerpts and update date.
      .map(thread => {
        const latestMessage = [...state.allMessages]
          .filter(message => message.thread_id === thread._id)
          .reduce((a, b) => (a.created > b.created ? a : b), {});

        return latestMessage.text
          ? {
              ...thread,
              excerpt: latestMessage.text,
              latestDate: latestMessage.created,
            }
          : thread;
      })
      .sort(sortThreadsByDate),
    threadMessages: state => [...state.allMessages]
      .filter(message => message.thread_id === state.currentThreadId)
      .map(message => {
        if (message.author === SELF_NAME) {
          message.self = true;
        }
        return message;
      }),
    threadExcerpt: state => text => text
    && text.length > EXCERPT_LENGTH
      ? text.substring(0, EXCERPT_LENGTH) + '...'
      : text,
    threadDate: state => date => dayjs(date).format('D MMMM YYYY'),
    messageDate: state => date => dayjs(date).format('DD.MM.YYYY HH:mm'),
  },
  mutations: {
    fakeLoaded(state, data) {
      state.fakeLoaded = data;
    },
    threadLoaded(state, data) {
      state.threadLoaded = data;
    },
    allThreads(state, data) {
      state.allThreads = data;
    },
    allMessages(state, data) {
      state.allMessages = data;
    },
    loadedThreads(state, data) {
      state.loadedThreads = data;
    },
    loadingThreads(state, data) {
      state.loadingThreads = data;
    },
    currentThreadId(state, data) {
      state.currentThreadId = data;
    },
    loadingMessages(state, data) {
      state.loadingMessages = data;
    },
    sendingMessage(state, data) {
      state.sendingMessage = data;
    },
    toggleSidebar(state, data) {
      state.showSidebar = typeof data === 'undefined'
        ? !state.showSidebar
        : data;
    },
  },
  actions: {
    generateFakeData({commit}) {
      // Generate threads data.
      const threads = [...Array(THREADS_AMOUNT)]
        .map(n => ({
          _id: faker.random.uuid(),
          subject: faker.lorem.words(),
          latestDate: faker.date.past(),
        }))
        .sort(sortThreadsByDate);
      commit('allThreads', threads);

      // Generate messages data.
      let messages = [];
      threads.forEach((thread, index) => {
        const thread_id = thread._id;
        const authors = [
          SELF_NAME,
          faker.name.firstName(),
        ];
        // Generate random amount of messages per thread.
        // 0 for the first thread.
        const messagesAmount = index === 0
          ? 0
          : Math.floor(Math.random() * MESSAGES_AMOUNT);
        [...Array(messagesAmount)].forEach((n, index) => {
          const message = {
            _id: faker.random.uuid(),
            thread_id,
            author: authors[Math.floor(Math.random() * authors.length)],
            text: faker.lorem.paragraph(),
          };

          // One message should be created at the same date as the thread.
          message.created = index === 0
            ? thread.latestDate
            : faker.date.past(1, thread.latestDate);

          messages.push(message);
        });
      });
      messages = messages.sort(sortMessagesByDate);
      commit('allMessages', messages);
      commit('fakeLoaded', true);
    },
    async addLoadedThreads({commit, state}) {
      commit('loadingThreads', true);
      await sleep(1000);
      commit('loadedThreads', state.loadedThreads + LOAD_AMOUNT);
      commit('loadingThreads', false);
    },
    async setCurrentThreadId({commit, state}, thread_id) {
      commit('loadingMessages', true);
      commit('currentThreadId', null);
      await sleep(1000);
      commit('currentThreadId', thread_id);
      commit('loadingMessages', false);
    },
    async sendMessage({commit, state}, {text}) {
      commit('sendingMessage', true);
      let {
        allMessages,
        currentThreadId,
      } = state;
      if (!allMessages
        || !Array.isArray(allMessages)
        || !currentThreadId
      ) {
        return;
      }
      await sleep(1000);
      allMessages.push({
        _id: faker.random.uuid(),
        author: SELF_NAME,
        thread_id: currentThreadId,
        created: new Date(),
        text,
      });
      commit('allMessages', allMessages);
      commit('sendingMessage', false);
    },
  },
};
