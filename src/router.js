import Vue from 'vue';
import Router from 'vue-router';
import Main from './views/Main.vue';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'main',
      component: Main,
    },
    {
      path: '/thread/:thread_id',
      name: 'thread',
      component: Main,
    },
    {
      path: '*',
      component: () => import('./views/404.vue'),
    },
  ],
});

router.afterEach(() => {
  window.scrollTo(0, 0);
});

export default router;
