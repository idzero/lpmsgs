/** @typedef {{}} module */
/** @typedef {{env: Object}} process */
module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: [
    'plugin:vue/essential',
    '@vue/airbnb',
  ],
  plugins: [
    // Show warnings instead of errors
    'only-warn'
  ],
  rules: {
    'max-len': ['warn', { 'code': 140 }],
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    // Disabled check because all code are committed with LF-style (\n)
    'linebreak-style': 'off',
    // Use unary operators ++ and --
    'no-plusplus': 'off',
    // Use two spaces for indention inside Vue script
    'vue/script-indent': ['warn', 2, { 'baseIndent': 1 }],
    // Can reassign parameters, using if parameter is undefined or for properties
    "no-param-reassign": "off",
    // Use `for of`
    "no-restricted-syntax": "off",
    // Use __ in TAP18n
    "no-underscore-dangle": "off",
    // Can use function before definition
    "no-use-before-define": "off",
  },
  overrides: [
    {
      'files': ['*.vue'],
      'rules': {
        'indent': 'off'
      }
    }
  ],
  parserOptions: {
    parser: 'babel-eslint',
  },
};
